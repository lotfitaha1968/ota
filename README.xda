[IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/iode_logo.png[/IMG]

[SIZE=5][B]Introduction[/B][/SIZE]

iodéOS is a privacy-focused operating system powered by LineageOS and based on the Android mobile platform. iodéOS aims at protecting the user's privacy with a built-in adblocker and by freeing the smartphone from snitches.

The objectives in the conception of this ROM are threefold:

[LIST=1]
[*]To keep the stability and security level of LineageOS, by minimizing the modifications made to the system. Apart the system modifications required by the adblocker, we mainly only added a few useful options commonly found in other custom ROMs, made some cosmetic changes, modified a few default settings to prevent data leaks to Google servers.
[*]To ease a quick adoption of this ROM by new users. We especially target users that are concerned by the protection of their privacy, but are not reluctant to still use inquisitive apps like Google ones. We thus included MicroG as well as a coherent set of default apps, and simplified the initial setup of the system. Particularly, an initialization of MicroG has been made with GCM notifications allowed by default, a privacy-friendly network location provider (DéjàVu) pre-selected, as well as Nominatim Geocoder.
[*]To provide a new and powerful way of blocking ads, malwares, data leaks of all kinds to many intrusive servers. We are developing an analyzer, tightly integrated into the system, that captures all DNS requests and network traffic, as well as a user interface (the iodé app). Compared to some other well-known adblockers, this has the advantages of:
[LIST]
[*]Avoiding to lock the VPN for that use. You can even use another adblocker that uses VPN technology alongside our blocker.
[*]Being independent of the kind of DNS server used by the system or set by an independent app: classical DNS on UDP port 53 or any other one, DNS over TLS (DoT), DNS over HTTPS (DoH), ..., as we capture the DNS requests before they are transmitted to the system function that emits the DNS request. What we do not support, is DoH when it is natively built into applications, i.e. when an app communicates directly with a DoH server, without asking name resolution to the system. It would require to decrypt HTTPS packets between such an app and the DoH server, which may create a big security hole.
[*]Precisely mapping DNS requests and network packets to the Android apps that emitted (or received) them.
[*]Deciding which apps have a filtered network usage (by default, all apps), and which ones can communicate with blacklisted servers.
[/LIST]
Since its first versions, we added many features to the iodé blocker: several levels of protection, fine-grained control over the hosts that should be blocked or authorized, displaying statistics on a map to see the quantity of data exchanged to which countries, clearing statistics... We are actively developing the blocker, and new functionalities will be regularly added.
[/LIST]

[B][SIZE=5]Features[/SIZE]

[SIZE=4]Changes in LineageOS to prevent data leaks:[/SIZE][/B]
[LIST]
[*]Default DNS server: Google's DNS replaced by Quad9's 'unblocked' servers in all parts of the system.
[*]A-GPS: patches to avoid leaking personnal information like IMSI to supl server.
[*]Captive portal login: connectivitycheck.gstatic.com replaced by captiveportal.kuketz.de for connectivity check.
[*]Dialer: Google default option replaced by OpenStreetMap for phone number lookup.
[/LIST]

[B][SIZE=4]Pre-installed apps:[/SIZE][/B]

We included many useful default apps, but our choice cannot suit everyone; so we added the possibility to remove them. It can be done at the end of the phone setup, or at any time by going to Parameters -> Apps & Notifications -> Preinstalled apps.

[LIST]
[*]MicroG core apps: GmsCore, GsfProxy, FakeStore.
[*]NLP backends for MicroG : DejaVuNLPBackend (default), MozillaNLPBackend, AppleNLPBackend, RadioCellsNLPBackend, Nominatim Geocoder.
[*]App stores : FDroid (with F-Droid Privileged Extension) and Aurora Store.
[*]Browser: our own fork of Firefox (with Qwant as default search engine, many other ones added, telemetry disabled, parts of telemetry code removed) instead of Lineage’s default browser Jelly.
[*]SMS: QKSMS instead of Lineage's default SMS app.
[*]Email: p≡p (Pretty Easy Privacy).
[*]Camera: our own fork of Open Camera, with a few tweaks.
[*]Maps/navigation: Magic Earth GPS & Navigation (the only one free but not open source).
[*]Keyboard: OpenBoard instead of AOSP keyboard.
[*]PDF: Pdf Viewer Plus.
[*]Personnal notes: Carnet.
[*]{Ad/Malware/Data leak}-blocker: iodé.
[*]News: to keep users informed about our developments, as well as a FAQ.
[*]Meteo: Geometric Weather.
[/LIST]

[B][SIZE=4]Pre-included FDroid repository:[/SIZE][/B]

The apps that we tweak or develop (microG services, the browser based on Firefox, the News app, Open Camera ...) are available through a repository that we included in FDroid (check the "Apps for iodéOS" category). For this purpose and to avoid name conflicts of some apps, we also had to make a few changes in FDroid.

[B][SIZE=4]Useful options from other custom ROMs:[/SIZE][/B]
[LIST]
[*]Smart charging (disables charging when a given level is reached, to protect battery health).
[*]Fingerprint vibration toggle.
[*]Swipe down to clear all in recent apps (Android 10 only).
[/LIST]

[B][SIZE=5]Installation Instructions[/SIZE][/B]

To download and flash our latest build, see [URL]https://gitlab.com/iode/ota[/URL].
You can also find [URL='https://forum.xda-developers.com/t/rom-fp4-11-0-iodeos-lineageos-18-1-microg-adblocker-02-02-2022.4397481/#post-86361819']here[/URL] direct links to the latest builds.

[B][SIZE=5]Supported devices[/SIZE][/B]
[LIST]
[*][URL='https://forum.xda-developers.com/t/rom-2e-11-0-iodeos-lineageos-18-1-microg-adblocker-14-09-2021.4333641/']Teracube 2e[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-fp3-10-0-iodeos-lineageos-17-1-microg-adblocker-07-03-2021.4243777']Fairphone FP3/FP3+[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-fp4-11-0-iodeos-lineageos-18-1-microg-adblocker-02-02-2022.4397481/']Fairphone FP4[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-a5y17lte-10-0-iodeos-lineageos-17-1-microg-adblocker-21-12-2020.4221605/']Samsung Galaxy A5/A7 2017 (a5j17lte/a7j17lte)[/URL]
[*][URL='https://forum.xda-developers.com/galaxy-s9/samsung-galaxy-s9--s9-cross-device-development/rom-iodeos-lineageos-17-1-microg-t4170059/post83610691#post83610691']Samsung Galaxy S9/S9+ (starlte/star2lte)[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-note9-10-0-iodeos-lineageos-17-1-microg-adblocker-25-12-2020.4207967/']Samsung Galaxy Note 9 (crownlte)[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-s10e-s10-s10-11-0-iodeos-lineageos-18-1-microg-adblocker-13-08-2021.4319203']Samsung S10e/S10/S10+ (beyond{0,1,2}lte)[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-note-10-10-11-0-iodeos-lineageos-18-1-microg-adblocker-13-08-2021.4319205']Samsung Note 10 (d1)[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-note-10-10-11-0-iodeos-lineageos-18-1-microg-adblocker-13-08-2021.4319209']Samsung Note 10+ (d2s)[/URL]
[*][URL='https://forum.xda-developers.com/xperia-xa2/development/rom-iodeos-lineageos-17-1-microg-t4138575/post83145219#post83145219']Sony Xperia XA2 (pioneer)[/URL]
[*][URL='https://forum.xda-developers.com/xperia-xz1/development/rom-iodeos-lineageos-17-1-microg-t4129997']Sony Xperia XZ1 (poplar)[/URL]
[*][URL='https://forum.xda-developers.com/xperia-xz2/development/rom-iodeos-lineageos-17-1-microg-t4138579']Sony Xperia XZ2 (akari)[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-stock-xz3-11-0-iodeos-lineageos-18-1-microg-adblocker-26-06-2021.4297297']Sony Xperia XZ3 (akatsuki)[/URL]
[*][URL='https://forum.xda-developers.com/Mi-9/development/rom-iodeos-lineageos-17-1-microg-t4124631']Xiaomi Mi9 (cepheus)[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-apollon-11-0-iodeos-lineageos-18-1-microg-adblocker-03-02-2022.4397819']Xiaomi Mi 10T 5G / Mi 10T Pro 5G[/URL]
[*][URL='https://forum.xda-developers.com/t/rom-monet-11-0-iodeos-lineageos-18-1-microg-adblocker-03-02-2022.4397823']Xiaomi Mi 10 Lite 5G[/URL]
[/LIST]

[B][SIZE=5]Sources[/SIZE][/B]
[LIST]
[*]iodéOS:  [URL]https://gitlab.com/iode/os[/URL]
[*]LineageOS: [URL]https://github.com/lineageos[/URL]
[*]device tree: [URL]https://gitlab.com/iode/os/devices/fairphone/device_fairphone_FP4[/URL]
[*]kernel: [URL]https://github.com/WeAreFairphone/android_kernel_fairphone_sm7225[/URL]
[/LIST]

[B][SIZE=5]Bug Reporting[/SIZE][/B]

You can post a message in this thread or (preferred) open an issue [URL='https://gitlab.com/iode/ota/-/issues']here[/URL].

[B][SIZE=5]Credits[/SIZE][/B]

LineageOS is a free, community built, aftermarket firmware distribution of android, which is designed to increase performance and reliability over stock android for your device.
All the source code for LineageOS is available in the [URL='https://github.com/lineageos/']LineageOS Github repo[/URL]. If you would like to contribute to LineageOS, please visit [URL='https://wiki.lineageos.org/']their Wiki[/URL] for more details.
This ROM would be nothing without the tremendous work made on [URL='https://microg.org/']MicroG[/URL], and all the other open source apps that we included. We are very grateful to their authors.


[B][SIZE=5]Contributors[/SIZE]

Direct contributors:[/B] [user=10894185]@iodeOS[/user], [user=7932843]@vince31fr[/user]
[B]Indirect contributors (too numerous to list):[/B] All the people that contributed to the device tree,  to LineageOS, and to the included open source apps.

[B][SIZE=5]Sponsoring[/SIZE][/B]

You can help in the development of this ROM by paying us a coffee here: [URL]https://paypal.me/iodeOS[/URL].

[B][SIZE=5]Screenshots[/SIZE][/B]

[IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/iode_home1.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/iode_home2.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/iode_drawer.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/iode_preinstalled.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_home.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_report.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_stream.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_map.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_lists.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_blocked.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_block_host.png[/IMG][IMG]https://gitlab.com/iode/ota/-/raw/master/pictures/mini/blocker_details_host.png[/IMG]
